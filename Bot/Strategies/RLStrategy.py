# -*- coding: utf-8 -*-
"""

@author: shoshi
"""

# Python3.4*

from Bot.Strategies.AbstractStrategy import AbstractStrategy
from Bot.Game.Piece import Piece
from Bot.Game.Field import Field
from Bot.Game.Player import Player
import copy




class RLStrategy(AbstractStrategy):
    def __init__(self, game):
        AbstractStrategy.__init__(self, game)
        self._actions = ['left', 'right', 'turnleft', 'turnright', 'down', 'drop']

    def choose(self):
        field = self._game.me.field
        piece = self._game.piece
        moves = []
        
        rotate = 0
        column = 0
        maxReward = -1

        for col in range(-3,10):
          piece._rotateIndex = 0
          for rotation in range(0,len(piece._rotations)):
            fNew, yPos= field.projectPieceDown(piece, [col, 0])
            if fNew == None:
              continue 
            score = self.reward(fNew, piece, [column, yPos])
            if score > maxReward:
              maxReward = score
              rotate = rotation
              column = col
            piece.turnLeft(rotate)
        
        for x in range(0,rotate):
          moves.append('turnleft')
        

        for moveRightLeft in range(0, abs(self._game.piecePosition[0] - column)):
          if (self._game.piecePosition[0] > column):
            moves.append('left')
          else:
            moves.append('right')
        moves.append('drop')
        return moves
        
    def reward(self, field, piece, offset):
        #combos = copy.deepcopy(self._game.me.Player.combo)
        #(7 * combos) +
        lines = 0
        newHole = 0
        cur = Field.offsetPiece(piece.positions(), offset)
        curHole = 1
        filled = int(self._game.round/15)
        colHeight = [0]*10
        for x,y in cur:
            while curHole+y < 20 and field[y+curHole][x] == 0:
                newHole += 1
                curHole += 1
        for col in range(0,10):
            for pos in range(0,20):
                colHeight[col] = 20-filled-pos
                
        maxHeight = max(colHeight)

        for line in range(20-filled-maxHeight,20-filled):
          if 0 not in field[line]:
            lines += 1
        
        meanHeight = sum(colHeight) / 10
        Score =  (-2 * newHole) + (8 * curHole) + (-3 * maxHeight) + (2* meanHeight) + (2 * lines)
        return Score
    
    '''
    def tryAll(self):
        maxReward = -1
        
        tryBoard=[]
        maxInd = 0
        for x in self.game.width:
           cur = Piece._projectPieceDown(self, self.game.piece)
           if reward(self.game.Field,self.game.piece, x ) >= maxReward:
               maxReward = reward(self.game.Field,self.game.piece, x )
               maxInd = x
               tryBoard.append(cur)
        return tryBoard[maxInd]                 
    '''